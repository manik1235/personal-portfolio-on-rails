Rails.application.routes.draw do
  get 'personal_portfolio/portfolio'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  #root 'application#hello'
  root 'personal_portfolio#portfolio'
end
